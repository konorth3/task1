#ifndef PLATFORM
#define PLATFORM
	#include "network.h"
	#ifdef _WIN32
		#include <winsock2.h>
		#include <ws2tcpip.h>
		#include <windows.h>
	#elif __linux__	
		#include <sys/types.h>
		#include <sys/socket.h>
		#include <netinet/in.h>
		#include <arpa/inet.h>
	#endif

	void preInit(){
		#ifdef _WIN32
			WSADATA wsaData;
			WSAStartup(0x0202, &wsaData);
		#elif __linux__
		#endif
	}
	
	void closeSoket(int socket){
		#ifdef _WIN32
			closesocket(socket);
			WSACleanup();			
		#elif __linux__
			close(thisSocket);
		#endif
	}
#endif

