#ifndef TASK1
#define TASK1
	/*
	there may be more functions
	*/

	void runServer (void){
		/*
		- maybe not void return 
		- maybe not void argument 
		- will run a server
		*/
	}

	void runClient (void){
		/*
		- maybe not void return 
		- maybe not void argument 
		- will run a client
		*/
	}

	void sndmsg (const char* str){
		/*
		- maybe not void return function
		- there will be more than one argument
		- will send a message between the server and the client
		*/
	}

	void crtprss (const char* str){
		/*
		- maybe not void return function
		- maybe there will be more than one argument
		- will create a process on client
		*/
	}

	void killprss (const char* str){
		/*
		- maybe not void return function
		- maybe there will be more than one argument
		- will kill a process on client
		*/
	}
#endif