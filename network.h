#ifndef NETWORK
#define NETWORK
	#include "platform.h"	
	#include <time.h>
	#include <pthread.h>
	
#include <string.h>
	
#include <stdio.h>
	
	const char HOST[] = "127.0.0.1";
	const int PORT = 13374;
	const int BUFSIZE = 512;	
	
	void asleep(int s){
		clock_t start = clock ();
		int i = 0 ,j = -1;
		for (; i < s;){
			i=(clock()-start)/CLOCKS_PER_SEC;
			if(i != j){
				j = i;
			}		
		}
	}	
	
	struct info{	
		int *socket;
		int code;
		char start;
		char end;
		const char* str;
	};
	
	void* startprs(void* info){
		struct info *inf = (struct info*) info;		
		inf->start = 1;		
		inf->code = system(inf->str);
		inf->end = 1;
	}
	
	void* sttalk(void* info){
		struct info *inf = (struct info*) info;		
		asleep(1);	
		if(inf->code == -1 && !inf->end)
			send(*(inf->socket), "success", BUFSIZE, 0);
		else if(inf->code != -1){
			send(*(inf->socket), "failure", BUFSIZE, 0);
			return (void*) 0;
		}
		while(!inf->end){
			asleep(1);
			if(inf->code == -1){
				send(*(inf->socket), "Running", BUFSIZE, 0);			
			}
		}
		send(*(inf->socket), "Crashed", BUFSIZE, 0);
		return (void*) 0;
	}

	void snitch (struct info *infotr, int* socket){
		pthread_t thread;
		pthread_attr_t th_attr;
		pthread_attr_init(&th_attr);
		infotr->socket = socket;
		pthread_create(&thread,&th_attr,sttalk,(void*)infotr);
		pthread_detach(thread);	
	}

	void crtprss (struct info *infotr, const char* str){
		pthread_t thread;
		pthread_attr_t th_attr;
		pthread_attr_init(&th_attr);
		infotr->str = &str[2];
		infotr->start = 0;		
		infotr->code = -1;
		infotr->end = 0;
		pthread_create(&thread,&th_attr,startprs,(void*)infotr);
		pthread_detach(thread);		
		while(!infotr->start){};
	}

	void killprss (const char* str){
		char pre[] = "TASKKILL /IM ";
		char end[] = " /F";
		int lp = strlen(pre);
		int lst = strlen(&str[2]);
		char* result = malloc(lp+lst+4);
		int i=0;
		for (; i<lp; i++) result[i] = pre[i];
		for (; i<lp+lst; i++) result[i] = str[i-lp+2];
		for (; i<lp+lst+4; i++) result[i] = end[i-lp-lst];
		system(result);
		free(result);		
	}
	
	void run(struct info *infotr, const char* str){
		char msg[BUFSIZE];
		msg[0] = 'r';
		msg[1] = ' ';
		for(int i = 2; i < BUFSIZE; i++){
			msg[i] = str[i-2];
		}
		msg[BUFSIZE-1] = '\0';		
		send(*(infotr->socket), msg, BUFSIZE, 0);
	};
	
	
	void kill(struct info *infotr, const char* str){
		char msg[BUFSIZE];
		msg[0] = 'k';
		msg[1] = ' ';
		for(int i = 2; i < BUFSIZE; i++){
			msg[i] = str[i-2];
		}
		msg[BUFSIZE-1] = '\0';
		send(*(infotr->socket), msg, BUFSIZE, 0);
	};
	
	
	void execute(struct info *infotr, char* str, int* socket){
		while (recv(*socket, str, BUFSIZE, 0) > 0){
			if (str[0] == 'r') {
				crtprss (infotr, str);
				snitch(infotr,socket);
			}
			else if (str[0] == 'k') killprss (str);
			
		}
	};
	
	void echo(struct info *infotr, char buf[]){
		while(recv(*(infotr->socket), buf, BUFSIZE, 0) > 0){
			printf("%s\n",buf);
		}
	};
	
#endif