#include "network.h"
#include "platform.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(){
	preInit();
	int thisSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (thisSocket < 0){
		printf("\nSocket Creation FAILED!");
		return 0;
	}		
	
	struct sockaddr_in destination;
	destination.sin_family = AF_INET;
	destination.sin_port = htons (PORT);
	destination.sin_addr.s_addr = INADDR_ANY;
	if (bind(thisSocket, (struct sockaddr *)&destination, sizeof(destination))<0){
		printf("\nBinding Socket FAILED!\n");
		if (thisSocket) closeSoket(thisSocket);
		return 0;
	}
	
	
	if (listen(thisSocket, 5)<0){
		printf("\nListening on Socket FAILED!\n");
		if (thisSocket) closeSoket(thisSocket);
		return 0;
	}
	
	struct sockaddr_in clientAddress;
	int clientSize = sizeof(clientAddress);
	thisSocket= accept(thisSocket, (struct sockaddr *)&clientAddress, (int *) &clientSize);
	if (thisSocket<0){
		printf("\nSocket Connection FAILED!\n");
		if (thisSocket) closeSoket(thisSocket);
		return 0;
	}
	
	
	char outbuf[BUFSIZE];	
	char inbuf[BUFSIZE];
	
	struct info infotr;
	infotr.socket = &thisSocket;
	run(&infotr, "notepad.exe");
	//kill(&infotr, "notepad.exe");
	echo(&infotr, inbuf);
	closeSoket(thisSocket);
}
